import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import { Header, Story,Content,C1,C2,C4,C5,C3 } from './index'

const App = () => {
  return (
    <View>
    <Header />
    <ScrollView>
      <Story />
      <Content />
      <C1/>
      <C2/>
      <C3/>
      <C4/>
      <C5/>
    </ScrollView>
    </View>
  )
}

export default App


import React from "react";
import {View, TouchableOpacity, Image, StyleSheet,} from "react-native";
import Icon from 'react-native-vector-icons/Feather';
import Icon1 from 'react-native-vector-icons/Ionicons';


const header =() =>{
  return(
    <View style={Setting.navBar}>
       <Image source= {require('../assets/ig.png')} style={{width: 130, height: 40}} />
       <View style={Setting.righNav}>
         <TouchableOpacity>
           <Icon style={Setting.NavItem} name="heart" size={30} />
         </TouchableOpacity>
         <TouchableOpacity>
         <Icon1 style={Setting.NavItem} name="ios-chatbubble-ellipses-outline" size={30} />
         </TouchableOpacity>
       </View>
       </View>   
  )
}


const Setting = StyleSheet. create({
  righNav: {
    marginVertical:5,
    flexDirection: 'row'
    
  },
  NavItem: {
    marginLeft: 20
  },
  navBar: {
    height: 50,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'space-between',
  },
});

export default header